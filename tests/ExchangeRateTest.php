<?php

namespace SalumIo\ExchangeRate;

use PHPUnit\Framework\TestCase;

class ExchangeRateTest extends TestCase
{
    protected $fetcher;

    protected function setUp()
    {
        $this->fetcher = new TmpFetcher('SEK');
    }

    public function test()
    {
        $exchangeRate = new ExchangeRate($this->fetcher, 'SEK');
        $rate = $exchangeRate->getRate('EUR');

        $this->assertInternalType('float', $rate);

        $this->assertSame(1, $exchangeRate->getRate('SEK'));
    }
}
