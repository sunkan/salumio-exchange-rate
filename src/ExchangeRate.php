<?php

namespace SalumIo\ExchangeRate;

class ExchangeRate
{
    protected $fetcher;
    protected $data;
    protected $base;

    public function __construct(FetcherInterface $fetcher, $base)
    {
        $this->fetcher = $fetcher;
        $this->base = $base;
    }

    public function getRate($code)
    {
        if (!$this->data) {
            $this->data = $this->fetcher->fetch();
        }

        $code = strtoupper($code);
        if (isset($this->data->rates->$code)) {
            return $this->data->rates->$code;
        } elseif ($code == $this->base) {
            return 1;
        }
    }
}