<?php

namespace SalumIo\ExchangeRate;

interface FetcherInterface
{
    public function fetch();
    public function setBase($base);
    public function getBase();
}