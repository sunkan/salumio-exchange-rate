<?php

namespace SalumIo\ExchangeRate;

class TmpFetcher extends Fetcher
{
    private $file = '../tmp/rates.json';
    public function fetch()
    {
        if (!file_exists($this->file)) {
            $data = parent::fetch();
            file_put_contents($this->file, json_encode($data));
            return $data;
        }

        return json_decode(file_get_contents($this->file));
    }
}