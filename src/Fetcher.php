<?php

namespace SalumIo\ExchangeRate;

use RuntimeException;

class Fetcher implements FetcherInterface
{
    protected $base;

    public function __construct($base)
    {
        $this->base = $base;
    }

    public function fetch()
    {
        $json = file_get_contents('http://api.fixer.io/latest?base=' . $this->base);
        if (!$json) {
            throw new RuntimeException("Could't fetch data from fixer.io");
        }
        $info = json_decode($json);
        if (!$json) {
            throw new RuntimeException("Could't parse json feed:\n" . json_last_error_msg());
        }

        return $info;
    }

    public function setBase($base)
    {
        $this->base = $base;
    }

    public function getBase()
    {
        return $this->base;
    }
}
